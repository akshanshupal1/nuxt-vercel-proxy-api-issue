import { joinURL } from 'ufo'

export default defineEventHandler(async (event) => {
  // Get the runtimeconfig proxy url
  let proxyUrl;
  // check the path
  let path = event.path;
  let target;
  if(path.includes('/abcd/')){
      proxyUrl = useRuntimeConfig().myProxyUrl1
     path = path.replace(/^\/api\/abcd/, '')
     target = joinURL(proxyUrl, path)
  }else{
    proxyUrl = useRuntimeConfig().myProxyUrl
    target = joinURL(proxyUrl, path)

  }
  return proxyRequest(event, target)
})